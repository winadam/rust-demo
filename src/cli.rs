use crate::address_book::{AddressBook, AddressBookEntry, AddressBookQuery};

pub enum Action {
    AddEntry(AddressBookEntry),
    RemoveEntries(AddressBookQuery),
    ListEntries(AddressBookQuery),
}

impl Action {
    pub fn run(self, book: &mut AddressBook) {
        match self {
            Action::AddEntry(entry) => book.add_entry(entry),
            Action::RemoveEntries(_query) => {}
            Action::ListEntries(query) => {
                book.search(&query).iter().for_each(|e| println!("{:?}", e))
            }
        }
    }
}

fn parse_entry(
    args: Vec<String>,
) -> (
    Result<String, String>,
    Result<String, String>,
    Result<String, String>,
) {
    let mut iter = args.iter();
    let name = iter
        .by_ref()
        .find(|s| *s == "--name")
        .ok_or("missing --name".to_string())
        .and_then(|_v| {
            iter.by_ref()
                .next()
                .ok_or("missing name".to_string())
                .map(|a: &String| a.clone())
        });

    let address = iter
        .by_ref()
        .find(|s| *s == "--address")
        .ok_or("missing --address".to_string())
        .and_then(|_v| {
            iter.by_ref()
                .next()
                .ok_or("missing address".to_string())
                .map(|a: &String| a.clone())
        });

    let phone_number = iter
        .by_ref()
        .find(|s| *s == "--phone-number")
        .ok_or("missing --phone-number".to_string())
        .and_then(|_v| {
            iter.by_ref()
                .next()
                .ok_or("missing phone_number".to_string())
                .map(|a: &String| a.clone())
        });

    (name, address, phone_number)
}

fn parse_query(args: Vec<String>) -> Result<AddressBookQuery, String> {
    let mut query = AddressBookQuery::new();
    let (name, address, phone_number) = parse_entry(args);

    if let Ok(name) = name {
        query = query.name_test(Box::new(move |other_name| name == *other_name));
    }

    if let Ok(address) = address {
        query = query.address_test(Box::new(move |other_address| address == *other_address));
    }

    if let Ok(phone_number) = phone_number {
        query = query.phone_number_test(Box::new(move |other_phone_number| {
            phone_number == *other_phone_number
        }));
    }

    Ok(query)
}

pub fn parse_args() -> Result<Action, String> {
    let args = std::env::args().collect::<Vec<String>>();
    match args.get(1).map(|s| s.as_ref()) {
        Some("add") => {
            let (name, address, phone_number) = parse_entry(args[1..].to_vec());
            let name = name?;

            let mut entry = AddressBookEntry::new(name);
            entry.set_address(address.ok());
            entry.set_phone_number(phone_number.ok());
            Ok(Action::AddEntry(entry))
        }
        Some("remove") => {
            let query = parse_query(args[1..].to_vec())?;
            Ok(Action::RemoveEntries(query))
        }
        Some("list") => {
            let query = parse_query(args[1..].to_vec())?;
            Ok(Action::ListEntries(query))
        }
        _ => Err("Unrecognized action".to_string()),
    }
}
