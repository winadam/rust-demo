//! Business logic for an AddressBook
#![allow(dead_code)]

use serde::{Deserialize, Serialize};
use std::convert::TryInto;
use std::fs::File;
use std::io::Write;

/// A collection of address book entries
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct AddressBook {
    entries: Vec<AddressBookEntry>,
}

impl AddressBook {
    pub fn new() -> Self {
        Self { entries: vec![] }
    }

    fn file_ext() -> &'static str {
        "addressbook"
    }

    /// Add an entry into the book
    pub fn add_entry(&mut self, entry: AddressBookEntry) {
        self.entries.push(entry);
    }

    /// Iterate through the entries in the book
    pub fn iter_entries(&self) -> impl Iterator<Item = &AddressBookEntry> {
        self.entries.iter()
    }

    /// Save the address book to a file
    pub fn save_to_file(&self, filename: &str) -> Result<(), String> {
        let database =
            serde_yaml::to_string(self).map_err(|_e| "serialize to string failed".to_string())?;
        let filename = format!("{}.{}", filename, Self::file_ext());
        let mut f =
            File::create(&filename).map_err(|_e| format!("failed to open file {}", &filename))?;
        f.write_all(database.as_bytes())
            .map_err(|_e| "writing to file failed".to_string())?;
        f.set_len(database.as_bytes().len().try_into().unwrap())
            .map_err(|_e| "setting file length failed".to_string())?;
        Ok(())
    }

    /// Load the address book from a file
    pub fn load_from_file(filename: &str) -> Result<Self, String> {
        let file = File::open(format!("{}.{}", filename, Self::file_ext()))
            .map_err(|_| format!("failed to open file {}", filename))?;
        serde_yaml::from_reader(file).map_err(|e| format!("{:?}", e))
    }

    /// Search the address book
    pub fn search(&self, query: &AddressBookQuery) -> Vec<&AddressBookEntry> {
        self.entries
            .iter()
            .filter(|e| query.test_entry(e))
            .collect()
    }
}

/// An entry for a person into an AddressBook
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct AddressBookEntry {
    name: String,
    address: Option<String>,
    phone_number: Option<String>,
}

impl AddressBookEntry {
    pub fn new<T: Into<String>>(name: T) -> Self {
        Self {
            name: name.into(),
            address: None,
            phone_number: None,
        }
    }

    /// Get the name
    pub fn get_name(&self) -> &String {
        &self.name
    }

    /// Set the address
    pub fn address<T: Into<String>>(mut self, address: T) -> Self {
        self.set_address(Some(address.into()));
        self
    }

    /// Set the phone number
    pub fn phone_number<T: Into<String>>(mut self, phone_number: T) -> Self {
        self.set_phone_number(Some(phone_number.into()));
        self
    }

    /// Set the address
    pub fn set_address(&mut self, address: Option<String>) {
        self.address = address;
    }

    /// Get the address
    pub fn get_address(&self) -> &Option<String> {
        &self.address
    }

    /// Set the phone number
    pub fn set_phone_number(&mut self, phone_number: Option<String>) {
        self.phone_number = phone_number;
    }

    /// Get the phone number
    pub fn get_phone_number(&self) -> &Option<String> {
        &self.phone_number
    }
}

/// A search query builder
pub struct AddressBookQuery {
    name_test: Option<Box<dyn Fn(&String) -> bool>>,
    address_test: Option<Box<dyn Fn(&String) -> bool>>,
    phone_number_test: Option<Box<dyn Fn(&String) -> bool>>,
}

impl AddressBookQuery {
    pub fn new() -> Self {
        Self {
            name_test: None,
            address_test: None,
            phone_number_test: None,
        }
    }

    /// Set the name predicate
    pub fn name_test(mut self, test: Box<dyn Fn(&String) -> bool>) -> Self {
        self.name_test = Some(test);
        self
    }

    /// Set the address predicate
    pub fn address_test(mut self, test: Box<dyn Fn(&String) -> bool>) -> Self {
        self.address_test = Some(test);
        self
    }

    /// Set the phone number predicate
    pub fn phone_number_test(mut self, test: Box<dyn Fn(&String) -> bool>) -> Self {
        self.phone_number_test = Some(test);
        self
    }

    /// Tests whether the entry matches
    pub fn test_entry(&self, entry: &AddressBookEntry) -> bool {
        if self.name_test.is_none()
            && self.address_test.is_none()
            && self.phone_number_test.is_none()
        {
            return true;
        }

        let mut is_the_one = false;
        if let Some(test) = &self.name_test {
            if test(entry.get_name()) {
                is_the_one = true;
            }
        }

        if let Some(test) = &self.address_test {
            if let Some(address) = entry.get_address() {
                if test(address) {
                    is_the_one = true;
                }
            }
        }
        if let Some(test) = &self.phone_number_test {
            if let Some(phone_number) = entry.get_phone_number() {
                if test(phone_number) {
                    is_the_one = true;
                }
            }
        }
        is_the_one
    }
}
