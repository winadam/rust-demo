mod address_book;
use address_book::AddressBook;

mod cli;

fn main() {
    let action = cli::parse_args().unwrap();
    let mut address_book =
        AddressBook::load_from_file("mybook").unwrap_or_else(|_e| AddressBook::new());
    action.run(&mut address_book);
    address_book.save_to_file("mybook").unwrap();
}
